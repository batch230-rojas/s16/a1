console.log("Hello");

let num1 = 25;
let num2 = 5;
console.log("The result of num1 + num2 should be 30.");
console.log("Actual Result:");
console.log(num1 + num2);


let num3 = 156;
let num4 = 44;
console.log("The result of num3 + num4 should be 200.");
console.log("Actual Result:");
console.log(num3 + num4);


let num5 = 17;
let num6 = 10;
console.log("The result of num5 - num6 should be 7.");
console.log("Actual Result:");
console.log(num5 - num6);


let minutesHour = 60;
let hoursDay = 24;
let daysWeek = 7;
let weeksMonth = 4;
let monthsYear = 12;
let daysYear = 365;
let resultMinutes = 60 * 24 * 365;
console.log("There are " + resultMinutes + " minutes in a year.")

let tempCelsius = 132;
let resultFahrenheit = (132*1.8) + 32;
console.log(tempCelsius + " degrees Celcius when converted to Farenheit is " + resultFahrenheit);

let num7 = 165 % 8;
let isDivisibleBy8 = 0;
console.log("The remainder of 165 divided by 8 is: " + num7);
console.log("Is num7 divisible by 8?");
//Log the value of isDivisibleBy8 in the console.
console.log(num7 === isDivisibleBy8);

let num8 = 348 % 4;
let isDivisibleBy4 = 0;
console.log("The remainder of 348 divided by 4 is: " + num8);
console.log("Is num8 divisible by 4?");
//Log the value of isDivisibleBy4 in the console.
console.log(num8 === isDivisibleBy4);






